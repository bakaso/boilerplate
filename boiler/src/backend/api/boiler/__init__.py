from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def create_app():
    app = Flask(__name__)
    app.config.from_object('config.Config')
    app.config.update(SERVER_NAME=config.HOST)
    app.config.update(SQLALCHEMY_DATABASE_URI=config.POSTGRES_URI)
    db.init_app(app)
    with app.app_context():
        from . import routes
        app.register_blueprint(routes.main_bp)
        db.create_all()
        return app


app = create_app()
