from . import db


class Boiler(db.Model):
    __tablename__ == 'boiler'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(500), nullable=False)
    uri = db.Column(db.String(100), nullable=False)
    personal = db.Column(db.Boolean, default=False)
