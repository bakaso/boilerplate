from flask import Blueprint
from flask import jsonify

from .models import Boiler
from .models import db


main_bp = Blueprint(
    'main_bp',
    __name__,
    subdomain='hell'  # since we host it on a subdomain
)


@main_bp.route('/api/getTop', methods=['GET'])
def get_top():
    boilers = Boiler.query.all(limit=10)
    return jsonify(boilers)


@main_bp.route('/api/getSuggestions', methods=['GET'])
def get_suggestions():
    name = request.args.get("name")
    if name is None:
        return '', 400
    boilers = Boiler.query.ilike(name=name)
    return jsonify(boilers)


@main_bp.route('/api/boiler', methods=['GET', 'POST'])
def boiler():
    if request.method == 'GET':
        uri = request.args.get('uri')
        if uri is None:
            return '', 400
        requested_boiler = Boiler.query.get(uri=uri)
        if requested_boiler is None:
            return '', 404
        return jsonify(requested_boiler)
    name = request.form.get("name")
    personal = request.form.get("personal")
    uri = request.form.get("uri")
    if name is None or personal is None or uri is None:
        return '', 400
    existing_boiler = Boiler.query.get(uri=uri[:100])
    if existing_boiler is not None:
        return '', 409
    new_boiler = Boiler(name=name[:500], uri=uri[:100], personal=personal)
    db.session.add(new_boiler)
    db.session.commit()
    return jsonify(new_boiler)
